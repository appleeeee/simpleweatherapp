package com.khimich.weatherapplication.data.api.model;

public class ForecastWeather {

    String day;
    int image;
    String degrees;

    public ForecastWeather(String city, int image, String degrees) {
        this.day = city;
        this.image = image;
        this.degrees = degrees;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDegrees() {
        return degrees;
    }

    public void setDegrees(String degrees) {
        this.degrees = degrees;
    }
}
