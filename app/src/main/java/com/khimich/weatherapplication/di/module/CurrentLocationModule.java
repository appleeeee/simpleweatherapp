package com.khimich.weatherapplication.di.module;

import android.location.Geocoder;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.khimich.weatherapplication.MyApp;

import java.util.Locale;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CurrentLocationModule {

    private final MyApp app;

    public CurrentLocationModule(MyApp app) {
        this.app = app;
    }


    @Provides
    @Singleton
    public GoogleApiClient provideGoogleApiClient() {
        return new GoogleApiClient.Builder(app)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    @Provides
    @Singleton
    public Geocoder providesGeoCoder(){
        return new Geocoder(app, Locale.getDefault());
    }
}
