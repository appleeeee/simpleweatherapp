package com.khimich.weatherapplication.di.component;

import com.khimich.weatherapplication.di.module.CurrentLocationModule;
import com.khimich.weatherapplication.ui.mainScreenWeather.MainWeatherPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = CurrentLocationModule.class)
public interface CurrentLocationComponent {

    void inject(MainWeatherPresenter mainWeatherPresenter);

}
