package com.khimich.weatherapplication.di.module;

import com.khimich.weatherapplication.common.utils.FragmentUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentUtilsModule {

    @Provides
    @Singleton
    FragmentUtils provideFragmentUtils(){
        return new FragmentUtils();
    }

}
