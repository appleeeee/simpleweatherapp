package com.khimich.weatherapplication.di.component;


import com.khimich.weatherapplication.di.module.FragmentUtilsModule;
import com.khimich.weatherapplication.ui.mainScreenWeather.MainWeatherActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {FragmentUtilsModule.class})
public interface FragmentUtilsComponent {

    void inject(MainWeatherActivity obj);
}
