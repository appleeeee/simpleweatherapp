package com.khimich.weatherapplication.common.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentUtils {

    public void changeFragment(FragmentManager fragmentManager, int container,
                               Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction()
                .replace(container, fragment);

        if(addToBackStack){
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        }

        fragmentTransaction.commit();
    }
}
