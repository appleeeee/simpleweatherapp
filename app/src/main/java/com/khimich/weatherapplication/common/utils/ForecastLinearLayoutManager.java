package com.khimich.weatherapplication.common.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

public class ForecastLinearLayoutManager extends LinearLayoutManager {

    public ForecastLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public boolean canScrollHorizontally() {
        return false;
    }
}
