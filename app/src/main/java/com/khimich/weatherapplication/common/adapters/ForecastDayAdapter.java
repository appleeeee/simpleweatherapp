package com.khimich.weatherapplication.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khimich.weatherapplication.R;
import com.khimich.weatherapplication.data.api.model.ForecastWeather;
import com.khimich.weatherapplication.ui.mainScreenWeather.MainWeatherFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastDayAdapter extends RecyclerView.Adapter<ForecastDayAdapter.ForecastDayViewHolder> {


    List<ForecastWeather> listForecast;


    public ForecastDayAdapter(List<ForecastWeather> listForecast) {
        this.listForecast = listForecast;
    }

    @Override
    public ForecastDayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_forecast, parent, false);
        view.setMinimumWidth(MainWeatherFragment.width/5);
        return new ForecastDayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastDayViewHolder holder, int position) {
        holder.bind(listForecast.get(position));
    }

    @Override
    public int getItemCount() {
        return listForecast.size();
    }

    public void setItems(List<ForecastWeather> listForecast) {
        this.listForecast = listForecast;

        notifyDataSetChanged();
    }

    class ForecastDayViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemForecastWeatherIcon)
        ImageView itemForecastWeatherIcon;
        @BindView(R.id.itemForecastWeatherDayTv)
        TextView itemForecastWeatherDayTv;
        @BindView(R.id.itemForecastWeatherDegreeTv)
        TextView itemForecastWeatherDegreeTv;


        public ForecastDayViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(ForecastWeather item) {
            itemForecastWeatherIcon.setImageResource(item.getImage());
            itemForecastWeatherDayTv.setText(item.getDay());
            itemForecastWeatherDegreeTv.setText(item.getDegrees());
        }


    }

}
