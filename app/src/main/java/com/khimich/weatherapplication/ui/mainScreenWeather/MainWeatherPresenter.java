package com.khimich.weatherapplication.ui.mainScreenWeather;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.khimich.weatherapplication.MyApp;
import com.khimich.weatherapplication.common.utils.Constants;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;


@InjectViewState
public class MainWeatherPresenter extends MvpPresenter<MainWeatherView> {

    @Inject
    GoogleApiClient apiClient;

    @Inject
    Geocoder geocoder;

    private String responseCity;

    public MainWeatherPresenter() {
        MyApp.getCurrentLocationComponent().inject(this);
        Log.d(Constants.TAG, "MainWeatherPresenter: ");

    }

    public void connentGoogleApiClient() {
        apiClient.connect();
        getLocation();
        Log.d(Constants.TAG, "connentGoogleApiClient: ");

    }

    public void disconnectGoogleApiClient() {
        apiClient.disconnect();
    }

    @SuppressWarnings({"MissingPermission"})
    public void getLocation() {
        apiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                Places.PlaceDetectionApi.getCurrentPlace(apiClient, null).setResultCallback
                        (placeLikelihoods -> {
                            Place place = placeLikelihoods.get(0).getPlace();
                            responseCity = getCity(place);
                        });
            }

            @Override
            public void onConnectionSuspended(int i) {
                getViewState().showHintForReload("Connection Failed. Try to reload");
            }
        });
    }

    private String getCity(Place place) {
        String city = "";
        Log.d(Constants.TAG, "getCity: ");
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
            if (addresses != null && addresses.size() > 0)
                city = addresses.get(0).getLocality();
            Log.d(Constants.TAG, "getCity: city " + city);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return city;
    }
}
