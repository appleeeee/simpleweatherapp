package com.khimich.weatherapplication.ui.mainScreenWeather;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.khimich.weatherapplication.MyApp;
import com.khimich.weatherapplication.R;
import com.khimich.weatherapplication.common.utils.Constants;
import com.khimich.weatherapplication.common.utils.FragmentUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static com.khimich.weatherapplication.common.utils.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;

public class MainWeatherActivity extends AppCompatActivity {

    @Inject
    FragmentUtils fragmentUtils;

    @BindView(R.id.parentContainer)
    LinearLayout parentContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_weather);
        MyApp.getFragmentUtilsComponent().inject(this);
        ButterKnife.bind(this);

        if (checkPermissions()) {
            Log.d(Constants.TAG, "checkPermissions onCreate: YES");
            changeFragment(null, getSupportFragmentManager());
        } else {
            Log.d(Constants.TAG, "checkPermissions onCreate: NO");
            requestPermissionWithRationale();
        }
    }

    private void changeFragment(Bundle savedInstanceState, FragmentManager fm) {
        Log.d(Constants.TAG, "changeFragment: ");
        if (savedInstanceState == null) {
            fragmentUtils.changeFragment(fm, R.id.weather_main_container, new MainWeatherFragment(), false);
        }
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(@NonNull String permission) {
        Log.d(Constants.TAG, "shouldShowRequestPermissionRationale: " + permission);
        return super.shouldShowRequestPermissionRationale(permission);
    }

    private boolean checkPermissions() {
        Log.d(Constants.TAG, "checkPermissions: ");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(Constants.TAG, "Permission Not Granted: ");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                Log.d(Constants.TAG, "grantResults ");

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    changeFragment(null, getSupportFragmentManager());
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                                || shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)) {
                            Toast.makeText(this, "You will not use the weather if your gps switch off", Toast.LENGTH_LONG).show();
                        } else {
                            showSnackBar();
                        }
                    }
                }
        }
    }

    private void showSnackBar() {
        Log.d(Constants.TAG, "showSnackBar ");
        Snackbar.make(parentContainer, "This application needs permissions from you.", Snackbar.LENGTH_LONG)
                .setAction("Settings", (v) -> {
                    openApplicationSettings();
                });

    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(Constants.TAG, "onActivityResult ");
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            changeFragment(null, getSupportFragmentManager());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void requestPermissionWithRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
            final String message = "Storage permission is needed to show files count";
            Snackbar.make(parentContainer, message, Snackbar.LENGTH_LONG)
                    .setAction("GRANT", v -> requestPerms())
                    .show();
        } else {
            requestPerms();
        }
    }

    private void requestPerms(){
        Log.d(Constants.TAG, "requestPerms ");
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                ACCESS_COARSE_LOCATION};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permissions,PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
}

