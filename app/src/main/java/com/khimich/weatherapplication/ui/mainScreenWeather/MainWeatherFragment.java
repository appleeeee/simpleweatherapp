package com.khimich.weatherapplication.ui.mainScreenWeather;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.khimich.weatherapplication.R;
import com.khimich.weatherapplication.common.adapters.ForecastDayAdapter;
import com.khimich.weatherapplication.common.utils.Constants;
import com.khimich.weatherapplication.common.utils.ForecastLinearLayoutManager;
import com.khimich.weatherapplication.data.api.model.ForecastWeather;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MainWeatherFragment extends MvpAppCompatFragment implements MainWeatherView {

    @InjectPresenter
    MainWeatherPresenter mainWeatherPresenter;

    Unbinder unbinder;

    public static int height;
    public static int width;


    @BindView(R.id.mainWeatherLocationTv)
    TextView mainWeatherLocationTv;
    @BindView(R.id.mainWeatherSettingsButton)
    ImageButton mainWeatherSettingsButton;
    @BindView(R.id.mainWeatherRecView)
    RecyclerView mainWeatherRecView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    private List<ForecastWeather> list;


    public MainWeatherFragment() {
    }

    public static MainWeatherFragment newInstance() {
        Bundle args = new Bundle();

        MainWeatherFragment fragment = new MainWeatherFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main_weather, container, false);
        unbinder = ButterKnife.bind(this, v);

        swipeRefresh.setOnRefreshListener(() -> {
            Log.d(Constants.TAG, "onRefresh: ");
            swipeRefresh.setRefreshing(false);

        });
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mainWeatherPresenter.connentGoogleApiClient();

        screenMeasurement();
        setAdapter();
    }


    @Override
    public void onStop() {
        super.onStop();
        mainWeatherPresenter.disconnectGoogleApiClient();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void loadData() {

    }

    @Override
    public void isConn() {

    }

    @Override
    public void showHintForReload(String error) {
        //TODO Show hint to reloading
    }

    @Override
    public void setRequestPermissions(String[] requestPermissions, int requestCode) {
        requestPermissions(requestPermissions, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void screenMeasurement() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        width = metrics.widthPixels;
        height = metrics.heightPixels;
    }

    private void setAdapter() {
        list = new ArrayList<>();
        ForecastDayAdapter forecastDayAdapter = new ForecastDayAdapter(list);
        mainWeatherRecView.setLayoutManager(new ForecastLinearLayoutManager(getActivity(),
                ForecastLinearLayoutManager.HORIZONTAL, false));


        mainWeatherRecView.setAdapter(forecastDayAdapter);
//        mainWeatherRecView.setNestedScrollingEnabled(false);

        list.add(new ForecastWeather("Monday", R.drawable.weather_lightning, getString(R.string.main_weather_page_degrees_c, "15")));
        list.add(new ForecastWeather("Tuesday", R.drawable.weather_fog, getString(R.string.main_weather_page_degrees_c, "1")));
        list.add(new ForecastWeather("Wednesday", R.drawable.weather_hail, getString(R.string.main_weather_page_degrees_c, "20")));
        list.add(new ForecastWeather("Thursday", R.drawable.weather_cloudy, getString(R.string.main_weather_page_degrees_c, "26")));
        list.add(new ForecastWeather("Friday", R.drawable.weather_snowy, getString(R.string.main_weather_page_degrees_c, "2")));

        forecastDayAdapter.setItems(list);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
