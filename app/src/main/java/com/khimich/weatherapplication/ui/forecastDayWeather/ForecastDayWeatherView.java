package com.khimich.weatherapplication.ui.forecastDayWeather;

import com.arellomobile.mvp.MvpView;

public interface ForecastDayWeatherView extends MvpView{

    void showProgress();

    void hideProgress();

    void loadData();

//    void shareWeather();

}
