package com.khimich.weatherapplication.ui.forecastDayWeather;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.khimich.weatherapplication.R;

import butterknife.ButterKnife;

public class ForecastDayWeatherFragment extends MvpAppCompatFragment implements ForecastDayWeatherView{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_day_forecast_weather, container, false);

        ButterKnife.bind(this, v);



        return v;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void loadData() {

    }
}
