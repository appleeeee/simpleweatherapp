package com.khimich.weatherapplication.ui.mainScreenWeather;

import com.arellomobile.mvp.MvpView;

public interface MainWeatherView extends MvpView {

    void showProgress();

    void hideProgress();

    void loadData();

    void isConn();

    void showHintForReload(String error);

    void setRequestPermissions(String[] requestPermissions, int requestCode);
}
