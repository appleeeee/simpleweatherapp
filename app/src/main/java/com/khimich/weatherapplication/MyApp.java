package com.khimich.weatherapplication;

import android.app.Application;

import com.khimich.weatherapplication.di.component.CurrentLocationComponent;
import com.khimich.weatherapplication.di.component.DaggerCurrentLocationComponent;
import com.khimich.weatherapplication.di.component.DaggerFragmentUtilsComponent;
import com.khimich.weatherapplication.di.component.FragmentUtilsComponent;
import com.khimich.weatherapplication.di.module.CurrentLocationModule;
import com.khimich.weatherapplication.di.module.FragmentUtilsModule;


public class MyApp extends Application {

    private static FragmentUtilsComponent fragmentUtilsComponent;
    private static CurrentLocationComponent currentLocationComponent;

    public static FragmentUtilsComponent getFragmentUtilsComponent(){
        return fragmentUtilsComponent;
    }

    public static CurrentLocationComponent getCurrentLocationComponent() {
        return currentLocationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        fragmentUtilsComponent = DaggerFragmentUtilsComponent.builder()
                .fragmentUtilsModule(new FragmentUtilsModule())
                .build();

        currentLocationComponent = DaggerCurrentLocationComponent.builder()
                .currentLocationModule(new CurrentLocationModule(this))
                .build();
    }
}
